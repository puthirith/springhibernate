import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.thirith.product.Product;
import com.thirith.product.ProductService;
import com.thirith.sale.Sale;
import com.thirith.sale.SaleService;


public class App {
	
	

	public static void main(String[] args){
		
		System.out.println("************** BEGINNING PROGRAM **************");
		
		listAllProduct();
		listAllSale();
		
//		out("\n\n");
//		insert();
		
	}
	
	public static void listAllSale(){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("main/resources/config/BeanLocations.xml");
		
		SaleService saleService = (SaleService) context.getBean("saleService");
		
		Sale sale = new Sale();
		
		List<Sale> sales = saleService.fetchAllSale();
		
		System.out.println(sales);
	}
	
/* ********************************* Product ***************************** */
	public static void listAllProduct(){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("main/resources/config/BeanLocations.xml");
		
		ProductService productService = (ProductService) context.getBean("productService");
		
		Product product = new Product();
		
		List<Product> products = productService.fetchAllPerson();
		
		System.out.println(products);
	}
	
	public static void insert(){
		ApplicationContext context = new ClassPathXmlApplicationContext("main/resources/config/BeanLocations.xml");
		ProductService productService = (ProductService) context.getBean("productService");
		
		Product product = new Product();
		
		Scanner in = getScannerObj();
		out("Please input\n");
		out("ID: ");
		product.setId(in.nextInt());
		out("Code: ");
		product.setCode(in.next());
		out("Name: ");
		product.setName(in.next());
		out("Category: ");
		product.setCategory(in.next());
		out("Price: ");
		product.setPrice(in.nextDouble());
//		productService.addProduct(product);
		productService.updateProduct(product);
		
		listAllProduct();
	}
	
	public static void out(String msg){
		System.out.print(msg);
	}
	
	public static Scanner getScannerObj(){
		return new Scanner(System.in);
	}
}
