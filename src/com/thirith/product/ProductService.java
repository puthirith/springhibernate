package com.thirith.product;

import java.util.List;

public class ProductService {

	private ProductDao productDao;
	
	public ProductDao getProductDao(){
		return productDao;
	}
	
	public void setProductDao(ProductDao productDao){
		this.productDao = productDao;
	}
	
	public void addProduct(Product product){
		getProductDao().insert(product);
	}
	
	public void updateProduct(Product product){
		getProductDao().update(product);
	}
	
	public List<Product> fetchAllPerson(){
		return getProductDao().selectAll();
	}
}
