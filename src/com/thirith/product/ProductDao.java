package com.thirith.product;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class ProductDao extends HibernateDaoSupport {

	public void insert(Product product){
		getHibernateTemplate().save(product);
	}
	
	public void update(Product product){
		getHibernateTemplate().update(product);
	}
	
	public List<Product> selectAll(){
		DetachedCriteria criteria = DetachedCriteria.forClass(Product.class);
		return getHibernateTemplate().findByCriteria(criteria);
	}
}
