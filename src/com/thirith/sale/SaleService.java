package com.thirith.sale;

import java.util.List;

public class SaleService {

	private SaleDao saleDao;
	
	public SaleDao getSaleDao(){
		return saleDao;
	}
	
	public void setSaleDao(SaleDao saleDao){
		this.saleDao = saleDao;
	}
	
	public void addSale(Sale sale){
		getSaleDao().insert(sale);
	}
	
	public void updateSale(Sale sale){
		getSaleDao().update(sale);
	}
	
	public List<Sale> fetchAllSale(){
		return getSaleDao().selectAll();
	}

}
