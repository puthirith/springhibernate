package com.thirith.sale;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public class SaleDao extends HibernateDaoSupport{

	public void insert(Sale sale){
		getHibernateTemplate().save(sale);
	}
	
	public void update(Sale sale){
		getHibernateTemplate().update(sale);
	}
	
	public List<Sale> selectAll(){
		DetachedCriteria criteria = DetachedCriteria.forClass(Sale.class);
		return getHibernateTemplate().findByCriteria(criteria);
	}
}
